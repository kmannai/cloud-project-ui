import React from 'react';
import './Clients.css';
import CustomAxiosInstance from '../../axios';

function Clients() {

  const [loading, setLoading] = React.useState<boolean>(true);
  const [clients, setClients] = React.useState<any[]>([]);
  const [firstName, setFirstName] = React.useState<string>('');
  const [lastName, setLastName] = React.useState<string>('');

  React.useEffect(() => {
    CustomAxiosInstance.get('/client')
      .then(res => {
        setClients(res.data);
      })
      .catch(err => {
        console.log(err);
      }).finally(() => {
        setLoading(false);
      });
  }, []);

  const handleAddClient = (e: any) => {
    if (firstName !== '' || lastName !== '') {
      let new_client: any = {
        first_name: firstName,
        last_name: lastName
      }
      CustomAxiosInstance.post('/client', new_client
      ).then(res => {
        new_client['client_id'] = res.data.id;
        setClients([...clients, new_client]);
      }).catch(err => {
        console.log(err);
      });
    }
  }

  if (loading) {
    return (
      <div className="Clients">
        <h3>Loading...</h3>
      </div>
    )
  }

  return (
    <div className="Clients">
      <div>
        <h3>Add Client : </h3>
        <form>
          <input type="text" placeholder="First Name" onChange={(e) => setFirstName(e.target.value)} />
          <input type="text" placeholder="Last Name" onChange={(e) => setLastName(e.target.value)} />
          <button type="button" onClick={handleAddClient}>Add</button>
        </form>
      </div>
      <div>
        <h3>List of Clients : </h3>
        <ul>
          {clients.map((client: any) => (
            <li key={client.id}>{client.first_name} {client.last_name}</li>
          ))}
        </ul>
      </div>
    </div>
  );
}

export default Clients;
