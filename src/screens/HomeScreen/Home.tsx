import logo from './logo.svg';
import './Home.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          PROJET CLOUD
        </p>
        <a
          className="App-link"
          href="./clients"
          target="_self"
          rel="noopener noreferrer"
        >
          Go to clients page
        </a>
      </header>
    </div>
  );
}

export default App;
