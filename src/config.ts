const baseURL = process.env.REACT_APP_API_BASE_URL || 'http://main-lb-265825901.us-east-1.elb.amazonaws.com:8080';

export default {
    baseURL,
};