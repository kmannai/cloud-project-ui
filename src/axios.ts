import axios, { AxiosInstance } from 'axios';
import config from './config';

const CustomAxiosInstance: AxiosInstance = axios.create({
    baseURL: config.baseURL,
    timeout: 10000,
    headers: {
        'Content-Type': 'application/json',
    },
});

export default CustomAxiosInstance;